//
//  EverydayHeroModelTests.swift
//  donator
//
//  Created by Onur Ersel on 24/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import XCTest

class EverydayHeroModelTests : XCTestCase {
    
    func testGetCharities() {

        let e = self.expectationWithDescription("get charities")
        
        EverydayHeroModel.getCharities(page: 1, limit: 1) { (success, charities) -> Void in
            
            if success {
                XCTAssertNotNil(charities)
                XCTAssertNotEqual(charities!.count, 0)
                e.fulfill()
            } else {
                XCTFail()
                e.fulfill()
            }
            
        }
        
        self.waitForExpectationsWithTimeout(30, handler: nil)
        
    }
    
    func testGetCharitiesLimit () {
        let e = self.expectationWithDescription("get charities limit")
        
        EverydayHeroModel.getCharities(page: 1, limit: 10) { (success, charities) -> Void in
            if success {
                XCTAssertNotNil(charities)
                XCTAssertEqual(charities!.count, 10)
                e.fulfill()
                
            } else {
                XCTFail()
                e.fulfill()
            }
        }
        
        self.waitForExpectationsWithTimeout(30, handler: nil)
    }
    
    
    func testGetCharitiesPage () {
        let e = self.expectationWithDescription("get charities page")
        
        var id1 : String?
        var id2 : String?
        var id3 : String?
        
        func t1 () {
            EverydayHeroModel.getCharities(page: 1, limit: 1) { (success, charities : [CharityModel]?) -> Void in
                if success {
                    XCTAssertNotNil(charities)
                    XCTAssertEqual(charities!.count, 1)
                    
                    id1 = charities![0].id
                    
                    t2()
                } else {
                    XCTFail()
                    e.fulfill()
                }
            }
        }
        func t2 () {
            EverydayHeroModel.getCharities(page: 1, limit: 1) { (success, charities : [CharityModel]?) -> Void in
                if success {
                    XCTAssertNotNil(charities)
                    XCTAssertEqual(charities!.count, 1)
                    
                    id2 = charities![0].id
                    
                    t3()
                } else {
                    XCTFail()
                    e.fulfill()
                }
            }
        }
        func t3 () {
            EverydayHeroModel.getCharities(page: 2, limit: 1) { (success, charities : [CharityModel]?) -> Void in
                if success {
                    XCTAssertNotNil(charities)
                    XCTAssertEqual(charities!.count, 1)
                    
                    id3 = charities![0].id
                    
                    t4()
                } else {
                    XCTFail()
                    e.fulfill()
                }
            }
        }
        func t4 () {
            XCTAssertEqual(id1!, id2!)
            XCTAssertNotEqual(id1!, id3!)
            XCTAssertNotEqual(id2!, id3!)
            e.fulfill()
        }
        
        t1()
        
        self.waitForExpectationsWithTimeout(30, handler: nil)
    }
    
}