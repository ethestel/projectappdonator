//
//  CharityModel.swift
//  donator
//
//  Created by Onur Ersel on 24/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit
import Alamofire

class CharityModel {
    
    typealias LogoRequestCallback = ((success:Bool, image:UIImage?)->Void)
    
    
    let causes : [Cause]
    let country_code : String
    let currency : Currency
    let description : String
    let donate_url : String
    let facebook_url : String
    let financial_context_id : String
    let get_started_url : String
    let gift_aid : String
    let id : String
    let image : ImageUrl
    let last_year_donation_total : LastYearDonationTotal
    let logo_url : String
    let merchant_id : String
    let merchant_name : String
    let name : String
    let page_count : String
    let phone : String
    let public_email : String
    let registration_number : String
    let registration_number_label : String
    let slug : String
    let tax_number : String
    let tax_number_label : String
    let twitter_url : String
    let url : String
    let website_url : String
    
    //logo image
    private var logoImage : UIImage?
    var logoImageRequest : Request?
    var logoRequestCallbacks = [LogoRequestCallback]()
    
    var causesString : String {
        var str = ""
        for i in 0..<causes.count{
            let c = causes[i]
            if i != 0 {
                str += " / "
            }
            str += c.name
        }
        
        return str
    }
    
    
    init (json : NSDictionary) {
        
        var cs = [Cause]()
        if let arr = json["causes"] as? [NSDictionary] {
            for obj in arr {
                let c = Cause(params: obj)
                cs.append(c)
            }
        }
        causes = cs
        
        country_code = (json["country_code"] as? String ?? "")
        
        if let j = json["currency"] as? NSDictionary {
            currency = Currency(params: j)
        } else {
            currency = Currency()
        }
        
        description = (json["description"] as? String ?? "")
        donate_url = (json["donate_url"] as? String ?? "")
        facebook_url = (json["facebook_url"] as? String ?? "")
        financial_context_id = (json["financial_context_id"] as? String ?? "")
        get_started_url = (json["get_started_url"] as? String ?? "")
        gift_aid = (json["gift_aid"] as? String ?? "")
        id = (json["id"] as? String ?? "")
        
        if let j = json["image"] as? NSDictionary {
            image = ImageUrl(params: j)
        } else {
            image = ImageUrl()
        }
        
        if let j = json["last_year_donation_total"] as? NSDictionary {
            last_year_donation_total = LastYearDonationTotal(params: j)
        } else {
            last_year_donation_total = LastYearDonationTotal()
        }
        
        logo_url = (json["logo_url"] as? String ?? "")
        merchant_id = (json["merchant_id"] as? String ?? "")
        merchant_name = (json["merchant_name"] as? String ?? "")
        name = (json["name"] as? String ?? "")
        page_count = (json["page_count"] as? String ?? "")
        phone = (json["phone"] as? String ?? "")
        public_email = (json["public_email"] as? String ?? "")
        registration_number = (json["registration_number"] as? String ?? "")
        registration_number_label = (json["registration_number_label"] as? String ?? "")
        slug = (json["slug"] as? String ?? "")
        tax_number = (json["tax_number"] as? String ?? "")
        tax_number_label = (json["tax_number_label"] as? String ?? "")
        twitter_url = (json["twitter_url"] as? String ?? "")
        url = (json["url"] as? String ?? "")
        website_url = (json["website_url"] as? String ?? "")
    }
    
    
    
    func requestLogo (completeCallback : LogoRequestCallback) {
        //return cached logo
        if let img = self.logoImage {
            completeCallback(success: true, image: img)
            return
        }
        
        //logo url is absent, nothing to wait for
        if logo_url.isEmpty {
            completeCallback(success: false, image: nil)
            return
        }
        
        //store callback request
        logoRequestCallbacks.append(completeCallback)
        
        //if currently downloading, wait
        if logoImageRequest != nil {
            return
        }
        
        logoImageRequest = Alamofire.request(.GET, logo_url).validate().responseData { response in
            if let data = response.data {
                //store image
                self.logoImage = UIImage(data: data)
                //callbacks
                for c in self.logoRequestCallbacks {
                    c(success: true, image: self.logoImage)
                }
            } else {
                //depressive callbacks
                for c in self.logoRequestCallbacks {
                    c(success: false/*:(*/, image: nil)
                }
            }
            
            self.logoImageRequest = nil
        }
    }
    
    
}