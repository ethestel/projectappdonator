//
//  ImageStruct.swift
//  donator
//
//  Created by Onur Ersel on 24/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit

struct ImageUrl {
    let extra_large_image_url : String
    let large_image_url : String
    let medium_image_url : String
    let original_image_url : String
    let small_image_url : String
    let tiny_image_url : String
    
    init () {
        extra_large_image_url = ""
        large_image_url = ""
        medium_image_url = ""
        original_image_url = ""
        small_image_url = ""
        tiny_image_url = ""
    }
    
    init (params : NSDictionary) {
        extra_large_image_url = (params["extra_large_image_url"] as? String  ??  "")
        large_image_url = (params["large_image_url"] as? String  ??  "")
        medium_image_url = (params["medium_image_url"] as? String  ??  "")
        original_image_url = (params["original_image_url"] as? String  ??  "")
        small_image_url = (params["small_image_url"] as? String  ??  "")
        tiny_image_url = (params["tiny_image_url"] as? String  ??  "")
    }
}
