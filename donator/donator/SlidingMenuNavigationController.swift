//
//  SlidingMenuViewController.swift
//  donator
//
//  Created by Onur Ersel on 23/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit
import Cartography
import FBSDKLoginKit

class SlidingMenuNavigationController : UINavigationController {
    
    
    private enum State {
        case Open, Close
    }
    
    private let animationDuration : NSTimeInterval = 0.25
    private var initialC = ConstraintGroup()
    private let openC = ConstraintGroup()
    private var state = State.Close
    private var closeOverlayButton : UIButton?
    
    private var menu : UIView?
    private var loginButton : UIButton?
    
    var hamburgerButton : UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.view.backgroundColor = UIColor.redColor()
        self.view.backgroundColor = UIColor.whiteColor()
        
        prepareCloseOverlay()
        prepareHamburgerButton()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.translatesAutoresizingMaskIntoConstraints = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        addConstraints()
        
        //i have to initiate menu on viewDidAppear instead of viewDidLoad; because at viewDidLoad, window is not ready yet, and menu adds itself to the window
        prepareMenu()
        
        self.view.translatesAutoresizingMaskIntoConstraints = true
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        removeConstraints()
    }
    
    
    /*****************************
     */
     //MARK: prepare
     /*
     *****************************/
    
    //close overlay
    private func prepareCloseOverlay () {
        closeOverlayButton = UIButton()
        self.view.addSubview(closeOverlayButton!)
        closeOverlayButton?.backgroundColor = UIColor.clearColor()
        closeOverlayButton?.hidden = true
        closeOverlayButton?.addTarget(self, action: "closeHandler", forControlEvents: .TouchUpInside)
        
        constrain (closeOverlayButton!, view) { closeOverlay, view in
            closeOverlay.width == view.width
            closeOverlay.height == view.height
            closeOverlay.top == view.top
            closeOverlay.left == view.left
        }
    }
    
    private func addCloseOverlay () {
        closeOverlayButton?.hidden = false
        view.bringSubviewToFront(closeOverlayButton!)
    }
    private func removeCloseOverlay () {
        closeOverlayButton?.hidden = true
    }
    
    
    //navigation bar button
    private func prepareHamburgerButton () {
        let hamburgerImage = UIImage(named: "hamburger")
        hamburgerButton = UIBarButtonItem(image: hamburgerImage, style: .Plain, target: self, action: "openHandler")
    }
    
    //menu
    private func prepareMenu () {
        
        if menu != nil {
            return
        }
        
        menu = UIView()
        menu?.backgroundColor = UIColor.blackColor()
        
        let window = UIApplication.sharedApplication().keyWindow!
        window.addSubview(menu!)
        window.sendSubviewToBack(menu!)
        
        constrain (menu!, window) { menu, window in
            menu.left == window.left
            menu.top == window.top
            menu.height == window.height
            menu.width == window.width
        }
        
        
        //login/logout button
        loginButton = UIButton()
        menu!.addSubview(loginButton!)
        loginButton!.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        loginButton!.titleLabel!.font = UIFont.systemFontOfSize(24)
        loginButton!.setTitle("Login", forState: .Normal)
        loginButton!.contentHorizontalAlignment = .Left
        loginButton!.addTarget(self, action: "loginHandler", forControlEvents: .TouchUpInside)
        constrain (loginButton!, menu!) { login, menu in
            login.width == menu.width - 10
            login.left == menu.left + 10
            login.top == menu.top + 20
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "accountStatusChangeHandler", name: FBSDKProfileDidChangeNotification, object: nil)
        
        updateLoginButtonText()
    }
    
    
    /*****************************
     */
     //MARK: handlers
     /*
     *****************************/
    @objc func closeHandler () {
        close ()
    }
    @objc func openHandler () {
        open ()
    }
    @objc func loginHandler () {
        login()
    }
    @objc func accountStatusChangeHandler () {
        updateLoginButtonText()
    }
    
    
    /*****************************
     */
     //MARK: actions
     /*
     *****************************/
    func open () {
        if state == .Open {
            return
        }
        state = .Open
        
        addCloseOverlay()
        openConstraints()
    }
    
    func close () {
        if state == .Close {
            return
        }
        state = .Close
        
        removeCloseOverlay()
        closeConstraints()
    }
    
    func toggle () {
        switch state {
        case .Open:
            close()
        case .Close:
            open()
        }
    }
    
    func login () {
        close()
        self.pushViewController(FacebookLoginViewController(), animated: true)
    }
    
    private func updateLoginButtonText () {
        UserModel.getUserProfile { (success : Bool, user : UserModel?) -> Void in
            if success {
                self.loginButton!.setTitle("Account", forState: .Normal)
            } else {
                self.loginButton!.setTitle("Login", forState: .Normal)
            }
        }
    }
    
    /*****************************
     */
     //MARK: constraints
     /*
     *****************************/
    
    private func addConstraints () {
        constrain (view, replace:initialC) { view in
            view.width == view.superview!.width
            view.height == view.superview!.height
            view.top == view.superview!.top
        }
    }
    
    private func removeConstraints () {
        constrain(clear: initialC)
    }
    
    

    private func openConstraints () {
        constrain (view, replace:openC) { view in
            view.left == view.superview!.left + 270
        }
        
        UIView.animateWithDuration(animationDuration, animations: view.layoutIfNeeded)
    }
    
    private func closeConstraints () {
        constrain (view, replace:openC) { view in
            view.left == view.superview!.left + 270
        }
        
        constrain (view, replace:openC) { view in
            view.left == view.superview!.left
        }
        
        UIView.animateWithDuration(animationDuration, animations: view.layoutIfNeeded)
    }
    

    
    
    
}
