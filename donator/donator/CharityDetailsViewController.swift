//
//  CharityDetailsViewController.swift
//  donator
//
//  Created by Onur Ersel on 29/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit
import Cartography
import QuartzCore
import Alamofire


class CharityDetailsViewController : UIViewController {
    
    var model : CharityModel?
    
    //info
    var scrollView : UIScrollView?
    var logoImageView : UIImageView?
    var nameLabel : UILabel?
    var imageView : UIImageView?
    var descTextView : UITextView?
    var phoneButton : UIButton?
    var moreInfoButton : UIButton?
    
    var inputTextField : UITextField?
    var currencyLabel : UILabel?
    var donationContainer : UIView?
    var tapOnViewGesture : UITapGestureRecognizer?
    var isKeyboardOpen : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.navigationItem.title = "Donate"
        
        prepareInformation()
        prepareDonationContainer()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        updateInformation()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        self.model = nil
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        addKeyboardListener()
        addTapListener()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        removeKeyboardListener()
        removeTapListener()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if let scroll = scrollView, let moreInfo = moreInfoButton {
            scroll.contentSize.height = moreInfo.frame.size.height + moreInfo.frame.origin.y + 10
        }
    }
    
    /*****************************
     */
     //MARK: prepare
     /*
     *****************************/
    
    func assignModel (model : CharityModel) {
        self.model = model
    }
    
    
    private func prepareDonationContainer () {
        
        //container
        donationContainer = UIView()
        self.view.addSubview(donationContainer!)
        
        constrain (donationContainer!, self.view) { container, view in
            container.width == view.width
            container.height == 200
            container.bottom == view.bottom
            container.left == view.left
        }
        
        //separator
        let separator = UIView()
        donationContainer!.addSubview(separator)
        separator.backgroundColor = UIColor.lightGrayColor()
        constrain(separator, donationContainer!) { separator, container in
            separator.left == container.left
            separator.right == container.right
            separator.top == container.top
            separator.height == 1
        }
        
        
        //amount input
        inputTextField = UITextField()
        donationContainer!.addSubview(inputTextField!)
        inputTextField!.clearButtonMode = .WhileEditing
        inputTextField!.textAlignment = .Center
        inputTextField!.adjustsFontSizeToFitWidth = true
        inputTextField!.keyboardType = .NumberPad
        inputTextField!.text = "50"
        inputTextField!.font = UIFont.systemFontOfSize(48)
        inputTextField!.layer.cornerRadius = 14
        inputTextField!.layer.borderColor = UIColor.darkGrayColor().CGColor
        inputTextField!.layer.borderWidth = 2
        inputTextField!.borderStyle = .Line
        constrain(inputTextField!, donationContainer!) { input, container in
            input.left == container.left + 50
            input.right == container.right - 50
            input.top == container.top + 10
        }
        
        currencyLabel = UILabel()
        donationContainer!.addSubview(currencyLabel!)
        currencyLabel!.adjustsFontSizeToFitWidth = true
        currencyLabel!.font = inputTextField!.font
        constrain(currencyLabel!, inputTextField!, donationContainer!) { label, input, container in
            label.left == input.right + 5
            label.top == input.top
            label.right == container.right
        }
        
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: "keyboardDoneHandler")
        let space = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: "")
        toolbar.items = [space, doneButton]
        inputTextField!.inputAccessoryView = toolbar
        
        
        // once/monthly
        let segment = UISegmentedControl(items: ["Once", "Monthly"])
        donationContainer!.addSubview(segment)
        segment.selectedSegmentIndex = 0
        segment.backgroundColor = UIColor.whiteColor()
        constrain (segment, inputTextField!, donationContainer!) { segment, input, container in
            segment.left == container.left + 50
            segment.right == container.right - 50
            segment.top == input.bottom + 10
        }
        
        //submit
        let submit = UIButton(type: .System)
        donationContainer!.addSubview(submit)
        submit.setTitle("DONATE", forState: .Normal)
        submit.addTarget(self, action: "donateHandler", forControlEvents: .TouchUpInside)
        constrain(submit, segment, donationContainer!) { submit, segment, container in
            submit.top == segment.bottom + 10
            submit.bottom == container.bottom - 10
            submit.left == segment.left
            submit.right == segment.right
        }
    }
    
    private func prepareInformation () {
        
        //scroll view
        scrollView = UIScrollView()
        self.view.addSubview(scrollView!)
        constrain(scrollView!, self.view) { scrollView, view in
            scrollView.left == view.left
            scrollView.top == view.top
            scrollView.width == view.width
            scrollView.height == view.height - 200
            
        }
        
        //image
        imageView = UIImageView()
        imageView?.backgroundColor = UIColor.lightGrayColor()
        scrollView!.addSubview(imageView!)
        imageView!.contentMode = .ScaleAspectFill
        imageView!.clipsToBounds = true
        constrain(imageView!, scrollView!) { image, scroll in
            image.top == scroll.top
            image.left == scroll.left
            image.width == scroll.width
            image.height == 100
        }
        
        
        //logo
        logoImageView = UIImageView()
        scrollView!.addSubview(logoImageView!)
        constrain(logoImageView!, imageView!, scrollView!) { logo, image, scrollView in
            logo.width == 60
            logo.height == 60
            logo.left == scrollView.left + 10
            logo.top == image.bottom + 10
        }
        
        //name
        nameLabel = UILabel()
        scrollView!.addSubview(nameLabel!)
        nameLabel!.font = UIFont.systemFontOfSize(18)
        nameLabel!.numberOfLines = 2
        constrain(nameLabel!, logoImageView!, scrollView!) { name, logo, scrollView in
            name.top == logo.top
            name.left == logo.right + 10
            name.height == logo.height
            name.width == scrollView.width - 90
        }
        
        //desc
        descTextView = UITextView()
        scrollView!.addSubview(descTextView!)
        descTextView!.font = UIFont.systemFontOfSize(12)
        descTextView!.scrollEnabled = false
        constrain(descTextView!, scrollView!, logoImageView!) { desc, scroll, logo in
            desc.width == scroll.width
            desc.top == logo.bottom + 10
            desc.left == scroll.left
            desc.height == 100
        }
        
        //phone
        phoneButton = UIButton()
        scrollView!.addSubview(phoneButton!)
        phoneButton!.titleLabel?.font = UIFont.systemFontOfSize(16)
        phoneButton!.setTitleColor(self.view.tintColor, forState: .Normal)
        phoneButton!.addTarget(self, action: "phoneHandler", forControlEvents: .TouchUpInside)
        constrain(phoneButton!, descTextView!) { phone, desc in
            phone.top == desc.bottom + 10
            phone.left == desc.left
            phone.width == desc.width
        }
        
        
        //more info
        moreInfoButton = UIButton()
        scrollView!.addSubview(moreInfoButton!)
        moreInfoButton!.titleLabel?.font = UIFont.systemFontOfSize(16)
        moreInfoButton!.setTitleColor(self.view.tintColor, forState: .Normal)
        moreInfoButton!.setTitle("More Info", forState: .Normal)
        moreInfoButton!.addTarget(self, action: "moreInfoHandler", forControlEvents: .TouchUpInside)
        constrain(moreInfoButton!, phoneButton!) { moreInfo, phone in
            moreInfo.top == phone.bottom + 10
            moreInfo.left == phone.left
            moreInfo.width == phone.width
        }
    }
    
    /*****************************
     */
     //MARK: update
     /*
     *****************************/
    
    
    private func updateInformation () {
        
        if let m = model {
            currencyLabel?.text = m.currency.iso_code
            
            loadImage(m.image.large_image_url)
            m.requestLogo(logoImageView!.showImageCallback)
            nameLabel?.text = m.name
            descTextView?.text = m.description
            phoneButton?.setTitle("Call \(m.name)", forState: .Normal)
        }
    }
    
    private func loadImage (urlStr : String) {
        Alamofire.request(.GET, urlStr).validate().responseData { response in
            if let data = response.data {
                
                //show image
                self.imageView!.image = UIImage(data: data)
                
            }
        }
    }
    
    
    /*****************************
     */
     //MARK: animations
     /*
     *****************************/
     
    private func moveScreenWhenKeyboardShows (keyboardHeight : CGFloat, animationDuration : NSTimeInterval) {
        UIView.animateWithDuration(animationDuration) {
            let screenBottomToInputBottom = self.donationContainer!.frame.size.height - self.inputTextField!.frame.size.height - self.inputTextField!.frame.origin.y - 10
            self.view.frame.origin.y = -keyboardHeight + screenBottomToInputBottom
        }
    }
    
    private func moveScreenWhenKeyboardHides (animationDuration : NSTimeInterval) {
        UIView.animateWithDuration(animationDuration) {
            self.view.frame.origin.y = 0
        }
    }
    
    
    /*****************************
     */
     //MARK: listeners
     /*
     *****************************/
    
    private func addKeyboardListener () {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShowHandler:", name: UIKeyboardWillShowNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHideHandler:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    private func removeKeyboardListener () {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    private func addTapListener () {
        tapOnViewGesture = UITapGestureRecognizer(target: self, action: "tapOnViewHandler")
        self.view.addGestureRecognizer(tapOnViewGesture!)
    }
    
    private func removeTapListener () {
        if let g = tapOnViewGesture {
            self.view.removeGestureRecognizer(g)
            tapOnViewGesture = nil
        }
    }
    
    /*****************************
    */
    //MARK: handlers
    /*
    *****************************/
    
    @objc func keyboardDoneHandler () {
        inputTextField?.resignFirstResponder()
    }
    
    @objc func keyboardWillShowHandler (notification : NSNotification) {
        if let info = notification.userInfo {
            let keyboardFrameValue = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
            let keyboardFrame = keyboardFrameValue.CGRectValue()
            let animationDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
            
            moveScreenWhenKeyboardShows(keyboardFrame.size.height, animationDuration: animationDuration)
        }
        
        isKeyboardOpen = true
    }
    
    @objc func keyboardWillHideHandler (notification : NSNotification) {
        if let info = notification.userInfo {
            let animationDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
            
            moveScreenWhenKeyboardHides(animationDuration)
        }
        
        isKeyboardOpen = false
    }
    
    @objc func tapOnViewHandler () {
        if isKeyboardOpen {
            inputTextField!.resignFirstResponder()
        }
    }
    
    @objc func phoneHandler () {
        if let m = model {
            
            let phoneNumber = m.phone.stringByReplacingOccurrencesOfString(" ", withString: "")
            guard let url = NSURL(string: "telprompt:\(phoneNumber)") else {
                print("no url")
                return
            }
            
            
            if UIApplication.sharedApplication().canOpenURL(url) {
                UIApplication.sharedApplication().openURL(url)
            }
            
        }
    }
    
    @objc func moreInfoHandler () {
        if let m = model where !m.url.isEmpty {
            guard let url = NSURL(string: m.url) else {return}
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    @objc func donateHandler () {
        
        UserModel.getUserProfile { (success : Bool, user : UserModel?) -> Void in
            
            if success {
                self.navigationController?.pushViewController(CardInfoViewController(), animated: true)
            } else {
                self.navigationController?.pushViewController(LoginToContinueViewController(), animated: true)
            }
            
        }
        
        
    }
    
}