//
//  LoadingIndicatorViewController.swift
//  donator
//
//  Created by Onur Ersel on 25/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit
import Cartography

class LoadingIndicatorView : UIView {
    
    static var instance : LoadingIndicatorView?
    static var constraintGroup : ConstraintGroup = ConstraintGroup()
    
    var activityIndicator : UIActivityIndicatorView?
    var isInitted = false
    
    class func show () {
        if instance != nil {
            return
        }
        
        if let window = UIApplication.sharedApplication().keyWindow {
            instance = LoadingIndicatorView()
            instance!.prepare()
            
            window.addSubview(instance!)
            instance!.activityIndicator?.startAnimating()
            
            constraintGroup = constrain (instance!, window) { instance, window in
                instance.width == window.width
                instance.height == window.height
                instance.top == window.top
                instance.left == window.left
            }
        }
    }
    
    class func hide () {
        if instance == nil {
            return
        }
        
        constrain (clear: constraintGroup)
        instance!.activityIndicator?.stopAnimating()
        instance!.removeFromSuperview()
        
        instance = nil
    }
    
    
    /*****************************
     */
     //MARK: prepare
     /*
     *****************************/
    
    private func prepare() {
        
        if isInitted {
            return
        }
        isInitted = true
        
        let overlay = UIView()
        self.addSubview(overlay)
        overlay.backgroundColor = UIColor.whiteColor()
        overlay.alpha = 0.8
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        self.addSubview(activityIndicator!)
        
        
        constrain(self, overlay, activityIndicator!) { container, overlay, activityIndicator in
            overlay.width == container.width
            overlay.height == container.height
            overlay.top == container.top
            overlay.left == container.left
            
            activityIndicator.center == container.center
        }
    }
    
}
