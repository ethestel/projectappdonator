//
//  CardInfoViewController.swift
//  donator
//
//  Created by Onur Ersel on 30/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit
import Cartography

class CardInfoViewController : UIViewController {
    
    private var viewBeingEditing : CardInfoTextInputView?
    
    private var containerScrollView : UIScrollView?
    private var cardNumberField : CardInfoTextInputView?
    private var cardOwnerField : CardInfoTextInputView?
    private var expirationDateField : CardInfoTextInputView?
    private var ccvField : CardInfoTextInputView?
    private var makePaymentButton : UIButton?
    private var currentResponder : UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.navigationItem.title = "Credit Card Information"
        
        prepareInputFields()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        resetValidationWarnings()
        self.navigationItem.title = "Card Information"
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        addKeyboardListener()
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeKeyboardListener()
    }
    
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if let container = containerScrollView, let button = makePaymentButton {
            container.contentSize.height = button.frame.size.height + button.frame.origin.y
        }
        
    }
    
    /*****************************
    */
    //MARK: prepare
    /*
    *****************************/
    
    
    private func prepareInputFields () {
        
        containerScrollView = UIScrollView()
        self.view.addSubview(containerScrollView!)
        constrain(containerScrollView!, self.view) { container, view in
            container.edges == view.edges
        }
        
        cardNumberField = CardInfoTextInputView()
        containerScrollView!.addSubview(cardNumberField!)
        cardNumberField!.prepareTextField("Card Number")
        cardNumberField!.changeCallback = updateCardNumber
        cardNumberField!.startEditingCallback = startEditingInput
        cardNumberField!.inputTf?.keyboardType = .NumberPad
        constrain (cardNumberField!, containerScrollView!) { input, container in
            input.width == container.width - 20
            input.height == 80
            input.top == container.top + 10
            input.left == container.left + 10
        }
        
        cardOwnerField = CardInfoTextInputView()
        containerScrollView!.addSubview(cardOwnerField!)
        cardOwnerField!.prepareTextField("Card Holder's Name")
        cardOwnerField!.changeCallback = updateCardHolderName
        cardOwnerField!.startEditingCallback = startEditingInput
        constrain(cardOwnerField!, cardNumberField!) { cardHolderName, input in
            cardHolderName.width == input.width
            cardHolderName.height == 80
            cardHolderName.top == input.bottom + 10
            cardHolderName.left == input.left
        }
        
        expirationDateField = CardInfoTextInputView()
        containerScrollView!.addSubview(expirationDateField!)
        expirationDateField!.prepareTextField("Expiration Date")
        expirationDateField!.prepareDatePicker()
        expirationDateField!.startEditingCallback = startEditingInput
        expirationDateField!.moveScreenCallback = moveScreenOnStartEditing
        expirationDateField!.endEditingCallback = endEditingInput
        constrain(expirationDateField!, cardOwnerField!) { date, cardHolderName in
            date.width == cardHolderName.width/2.0 - 5
            date.height == 80
            date.left == cardHolderName.left
            date.top == cardHolderName.bottom + 10
        }
        
        ccvField = CardInfoTextInputView()
        containerScrollView!.addSubview(ccvField!)
        ccvField!.prepareTextField("CCV2")
        ccvField!.inputTf?.keyboardType = .NumberPad
        ccvField!.changeCallback = updateCCV
        ccvField!.startEditingCallback = startEditingInput
        constrain(ccvField!, cardOwnerField!) { ccv, cardHolderName in
            ccv.width == cardHolderName.width/2.0 - 5
            ccv.height == 80
            ccv.right == cardHolderName.right
            ccv.top == cardHolderName.bottom + 10
        }
        
        
        makePaymentButton = UIButton()
        containerScrollView!.addSubview(makePaymentButton!)
        makePaymentButton!.setTitle("MAKE PAYMENT", forState: .Normal)
        makePaymentButton!.setTitleColor(self.view.tintColor, forState: .Normal)
        makePaymentButton!.addTarget(self, action: "makePaymentHandler", forControlEvents: .TouchUpInside)
        constrain(makePaymentButton!, ccvField!, cardNumberField!) { button, ccv, input in
            button.width == input.width
            button.height == 80
            button.left == input.left
            button.right == input.right
            button.top == ccv.bottom + 10
        }
        
        
        self.view.layoutIfNeeded()
    }
    
    
    /*****************************
     */
     //MARK: validation
     /*
     *****************************/
     
    private func validate () -> Bool {
        
        //card number
        let resCardNumber = validateCardNumber()
        if resCardNumber {
            cardNumberField?.hideWarning()
        } else {
            cardNumberField?.showWarning("Card number is invalid")
        }
        
        //card owner name
        let resCardOwner = validateOwnerName()
        if resCardOwner {
            cardOwnerField?.hideWarning()
        } else {
            cardOwnerField?.showWarning("Owner name is invalid")
        }
        
        //ccv
        let resCcv = validateCCV()
        if resCcv {
            ccvField?.hideWarning()
        } else {
            ccvField?.showWarning("Invalid ccv")
        }
        
        return resCardNumber  &&  resCardOwner  &&  resCcv
    }
    
    private func resetValidationWarnings () {
        
    }
    
    private func validateCardNumber () -> Bool {
        let number = cardNumberField!.inputTf!.text!
        let regexp = try? NSRegularExpression(pattern: "\\d", options: .CaseInsensitive)
        let count = regexp!.numberOfMatchesInString(number, options: .ReportProgress, range: NSMakeRange(0, number.characters.count))
        
        return count == 16
    }
    
    private func validateOwnerName () -> Bool {
        let name = cardOwnerField!.inputTf!.text!
        let regexp = try? NSRegularExpression(pattern: "\\w", options: .CaseInsensitive)
        let count = regexp!.numberOfMatchesInString(name, options: .ReportProgress, range: NSMakeRange(0, name.characters.count))
        
        return count > 0
    }
    
    private func validateCCV () -> Bool {
        let ccv = ccvField!.inputTf!.text!
        let regexp = try? NSRegularExpression(pattern: "\\d", options: .CaseInsensitive)
        let count = regexp!.numberOfMatchesInString(ccv, options: .ReportProgress, range: NSMakeRange(0, ccv.characters.count))
        
        return count == 3
    }
    
    
    /*****************************
    */
    //MARK: callbacks
    /*
    *****************************/
    
    
    func updateCardNumber (textfield : UITextField, range : NSRange, replaceWith : String) -> Bool {
        
        var str = (textfield.text ?? "")
        var newRange = NSRange(location: range.location, length: range.length)
        
        //skip deleting spaces
        let stringInRange = str.substringWithRange(newRange.stringRange(str))
        if newRange.length == 1  &&  stringInRange == " " {
            newRange.location--
            newRange.length++
        }
        
        str.replaceRange(newRange.stringRange(str), with: replaceWith)
        
        //clear up non numeric characters
        let regexp = try? NSRegularExpression(pattern: "\\D", options: .CaseInsensitive)
        str = regexp!.stringByReplacingMatchesInString(str, options: .ReportProgress, range: NSMakeRange(0, str.characters.count), withTemplate: "")
        
        //limit with 16 characters
        if str.characters.count > 16 {
            str = str.substringWithRange(Range<String.Index>(start: str.startIndex, end: str.startIndex.advancedBy(16)))
        }
        
        //format
        var i = 0
        var formattedStr = ""
        for c in str.characters {
            formattedStr.append(c)
            
            if ++i < str.characters.count  &&  i == 4 {
                i = 0
                formattedStr.append(Character(" "))
            }
        }
        
        textfield.text = formattedStr
        
        return false
    }
    
    func updateCardHolderName (textfield : UITextField, range : NSRange, replaceWith : String) -> Bool {
        
        var str = (textfield.text ?? "")
        str.replaceRange(range.stringRange(str), with: replaceWith.uppercaseString)
        
        //remove numbers and punctuations
        let regexp = try? NSRegularExpression(pattern: "\\d", options: .CaseInsensitive)
        str = regexp!.stringByReplacingMatchesInString(str, options: .ReportProgress, range: NSMakeRange(0, str.characters.count), withTemplate: "")
        
        textfield.text = str
        
        return false
    }
    
    func updateCCV (textfield : UITextField, range : NSRange, replaceWith : String) -> Bool {
        
        var str = (textfield.text ?? "")
        str.replaceRange(range.stringRange(str), with: replaceWith)
        
        //remove numbers and punctuations
        let regexp = try? NSRegularExpression(pattern: "\\D", options: .CaseInsensitive)
        str = regexp!.stringByReplacingMatchesInString(str, options: .ReportProgress, range: NSMakeRange(0, str.characters.count), withTemplate: "")
        
        //limit with 16 characters
        if str.characters.count > 3 {
            str = str.substringWithRange(Range<String.Index>(start: str.startIndex, end: str.startIndex.advancedBy(3)))
        }
        
        textfield.text = str
        
        return false
    }
    
    func startEditingInput (view : CardInfoTextInputView) {
        
        if currentResponder != nil {
            currentResponder!.resignFirstResponder()
        }
        currentResponder = view.inputTf
        
        viewBeingEditing = view
    }
    
    func moveScreenOnStartEditing (keyboardHeight : CGFloat, animationDuration : NSTimeInterval) {
        moveScreenWhenKeyboardShows(keyboardHeight, animationDuration: animationDuration)
    }
    
    func endEditingInput (animationDuration : NSTimeInterval) {
        currentResponder = nil
        moveScreenWhenKeyboardHides(animationDuration)
    }
     
     /*****************************
     */
     //MARK: animations
     /*
     *****************************/
    
    private func moveScreenWhenKeyboardShows (keyboardHeight : CGFloat, animationDuration : NSTimeInterval) {
        
        guard let view = viewBeingEditing else {return}
        
        let viewMidY = view.frame.origin.y + view.frame.size.height/2.0
        let screenMidY = (self.view.frame.size.height - keyboardHeight) / 2.0
        let diff = screenMidY - viewMidY
        
        //dont move if view is visible
        if diff >= 0 {
            return
        }
        
        UIView.animateWithDuration(animationDuration) {
            self.view.frame.origin.y = diff
        }
    }
    
    private func moveScreenWhenKeyboardHides (animationDuration : NSTimeInterval) {
        UIView.animateWithDuration(animationDuration) {
            self.view.frame.origin.y = 0
        }
    }
    
    
    /*****************************
     */
     //MARK: handlers
     /*
     *****************************/
     
    @objc func keyboardWillShowHandler (notification : NSNotification) {
        if let info = notification.userInfo {
            let keyboardFrameValue = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
            let keyboardFrame = keyboardFrameValue.CGRectValue()
            let animationDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
            
            moveScreenWhenKeyboardShows(keyboardFrame.size.height, animationDuration: animationDuration)
        }
    }
    
    @objc func keyboardWillHideHandler (notification : NSNotification) {
        if let info = notification.userInfo {
            let animationDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
            
            moveScreenWhenKeyboardHides(animationDuration)
        }
    }
    
    @objc func makePaymentHandler () {
        
        if validate() {
            self.navigationController?.pushViewController(ResultViewController(), animated: true)
        }
        
    }
    
    /*****************************
    */
    //MARK: listeners
    /*
    *****************************/
    
    private func addKeyboardListener () {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShowHandler:", name: UIKeyboardWillShowNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHideHandler:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    private func removeKeyboardListener () {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
}
