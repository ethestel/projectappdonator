//
//  CharityCollectionModel.swift
//  donator
//
//  Created by Onur Ersel on 25/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit

class CharityCollectionModel {
    
    let limit : Int
    var page : Int
    var charities : [CharityModel]
    
    var nextPage : Int {
        get {
            return page+1
        }
    }
    
    init (limit : Int) {
        self.limit = limit
        page = 0
        charities = [CharityModel]()
    }
    
    func reset () {
        page = 0
        charities.removeAll()
    }
    
    func addPage (charitiesOfPage : [CharityModel]) {
        charities.appendContentsOf(charitiesOfPage)
        page++
    }
    
    func getCharityWithIndexPath (index : NSIndexPath) -> CharityModel? {
        let i = index.indexAtPosition(1)
        if charities.count != 0  &&  i < charities.count {
            return charities[i]
        } else {
            return nil
        }
    }
}
