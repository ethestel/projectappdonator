//
//  LastYearDonationTotal.swift
//  donator
//
//  Created by Onur Ersel on 24/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit

struct LastYearDonationTotal {
    
    let cents : Int
    let currency : Currency
    
    
    init () {
        cents = 0
        currency = Currency()
    }
    
    init (params : NSDictionary) {
        cents = (params["cents"] as? Int  ??  0)
        
        if let c = params["currency"] as? NSDictionary {
            currency = Currency(params: c)
        } else {
            currency = Currency()
        }
    }
}
