//
//  CharityCell.swift
//  donator
//
//  Created by Onur Ersel on 29/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit
import Cartography

class CharityCell : UITableViewCell {
    
    static let height : CGFloat = 100
    
    weak var model : CharityModel?
    
    var titleLabel : UILabel?
    var descLabel : UILabel?
    var charityLogo : UIImageView?
    var donateLogo : UIImageView?
    var causesLabel : UILabel?
    
    func prepare () {
        
        //logo
        charityLogo = UIImageView()
        self.contentView.addSubview(charityLogo!)
        charityLogo!.contentMode = .ScaleAspectFit
        constrain(charityLogo!, self.contentView) { charityLogo, view in
            charityLogo.width == 60
            charityLogo.height == 60
            charityLogo.left == view.left + 10
            charityLogo.top == view.top + 10
        }
        
        
        //name
        titleLabel = UILabel()
        self.contentView.addSubview(titleLabel!)
        titleLabel!.font = UIFont.systemFontOfSize(18)
        constrain(titleLabel!, charityLogo!, self.contentView) { title, logo, view in
            title.top == logo.top
            title.left == logo.right + 10
            title.right == view.right - 10
        }
        
        //desc
        descLabel = UILabel()
        self.contentView.addSubview(descLabel!)
        descLabel!.numberOfLines = 2
        descLabel!.font = UIFont.systemFontOfSize(12)
        descLabel!.textColor = UIColor.grayColor()
        constrain (descLabel!, charityLogo!, titleLabel!, self.contentView) { desc, logo, title, view in
            desc.top == title.bottom + 5
            desc.left == logo.right + 10
            desc.right == view.right - 10
        }
        
        //causes
        causesLabel = UILabel()
        self.contentView.addSubview(causesLabel!)
        causesLabel!.font = UIFont.systemFontOfSize(10)
        causesLabel!.textColor = self.tintColor
        causesLabel!.text = "Denemejsgajkfg safalskf aksjfg asjkfga kjasgf akj"
        constrain (causesLabel!, self.contentView) { causes, view in
            causes.left == view.left + 10
            causes.right == view.right - 10
            causes.bottom == view.bottom - 10
        }
    }
    
    func assignModel (model : CharityModel) {
        self.model = model
        
        //title
        self.titleLabel!.text = model.name
        
        //desc
        self.descLabel!.text = model.description
        
        //causes
        self.causesLabel!.text = model.causesString
        
        //logo
        self.charityLogo!.hidden = true
        model.requestLogo(self.charityLogo!.showImageCallback)
    }
    
    
    
}
