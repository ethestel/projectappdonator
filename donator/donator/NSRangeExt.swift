//
//  NSRangeExt.swift
//  donator
//
//  Created by Onur Ersel on 30/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit

extension NSRange {
    
    func stringRange(str : String) -> Range<String.Index> {
        
        let loc = self.location
        let len = self.length
        
        let startIndex = str.startIndex.advancedBy(loc)
        let endIndex = startIndex.advancedBy(len)
        return Range<String.Index>(start: startIndex, end: endIndex)
    }
    
}
