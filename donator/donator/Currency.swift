//
//  CurrencyStruct.swift
//  donator
//
//  Created by Onur Ersel on 24/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit

struct Currency {
    let iso_code : String
    let name : String
    let symbol : String
    
    init () {
        iso_code = ""
        name = ""
        symbol = ""
    }
    
    init (params : NSDictionary) {
        iso_code = (params["iso_code"] as? String  ??  "")
        name = (params["name"] as? String  ??  "")
        symbol = (params["symbol"] as? String  ??  "")
    }
}
