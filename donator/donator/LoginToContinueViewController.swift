//
//  LoginToContinueViewController.swift
//  donator
//
//  Created by Onur Ersel on 05/01/16.
//  Copyright © 2016 Onur Ersel. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Cartography

class LoginToContinueViewController : UIViewController {
    
    private var loginButton : FBSDKLoginButton?
    private var continueButton : UIButton?
    private var infoLabel : UILabel?
    private var shouldRemoveFromControllerTree : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepare()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "Donate"
        self.navigationItem.backBarButtonItem?.title = "Back"
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        addListeners()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeListeners()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        if shouldRemoveFromControllerTree {
            
            if let index = self.navigationController?.viewControllers.indexOf(self) {
                self.navigationController?.viewControllers.removeAtIndex(index)
            }
            
        }
    }
    
    
    /*****************************
    */
    //MARK: prepare
    /*
    *****************************/
    
    
    private func prepare () {
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        //info label
        infoLabel = UILabel()
        self.view.addSubview(infoLabel!)
        infoLabel!.numberOfLines = 2
        infoLabel!.textAlignment = .Center
        infoLabel!.text = "Please log in to continue"
        
        
        //login button
        loginButton = FBSDKLoginButton()
        self.view.addSubview(loginButton!)
        loginButton!.loginBehavior = .Native
        loginButton!.readPermissions = ["public_profile"]
        
        //continue button
        continueButton = UIButton()
        self.view.addSubview(continueButton!)
        continueButton!.setTitleColor(self.view.tintColor, forState: .Normal)
        continueButton!.setTitle("Continue to Payment", forState: .Normal)
        continueButton!.addTarget(self, action: "continueToPaymentHandler", forControlEvents: .TouchUpInside)
        continueButton!.hidden = true
        
        constrain (infoLabel!, loginButton!, continueButton!, self.view) { info, login, continueButton, view in
            login.center == view.center
            
            info.centerX == view.centerX
            info.bottom == login.top - 20
            info.width == view.width - 30
            
            continueButton.centerX == view.centerX
            continueButton.bottom == view.bottom - 15
            continueButton.width == view.width
        }
    }
    
    
    
    /*****************************
     */
     //MARK: actions
     /*
     *****************************/
     
    private func showContinue () {
        continueButton?.hidden = false
        continueButton?.alpha = 0
        
        UIView.animateWithDuration(0.7, animations: {
            self.continueButton?.alpha = 1
        })
    }
    
    private func hideContinue () {
        continueButton?.alpha = 0
        continueButton?.hidden = true
    }
    
    
    /*****************************
     */
     //MARK: handlers
     /*
     *****************************/
     
    @objc func facebookProfileChangeHandler () {
        
        if FBSDKAccessToken.currentAccessToken() != nil {
            showContinue()
        } else {
            hideContinue()
        }
        
    }
    
    @objc func continueToPaymentHandler () {
        shouldRemoveFromControllerTree = true
        self.navigationController?.pushViewController(CardInfoViewController(), animated: true)
    }
    
    /*****************************
    */
    //MARK: listeners
    /*
    *****************************/
    
    private func addListeners () {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "facebookProfileChangeHandler", name: FBSDKProfileDidChangeNotification, object: nil)
        
        facebookProfileChangeHandler()
    }
    
    private func removeListeners () {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    
}
