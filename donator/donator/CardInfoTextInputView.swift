//
//  CardInfoTextInputView.swift
//  donator
//
//  Created by Onur Ersel on 30/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit
import Cartography

class CardInfoTextInputView : UIView, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let animationDuration : NSTimeInterval = 0.25
    
    var titleLabel : UILabel?
    var inputTf : UITextField?
    var warningLabel : UILabel?
    
    private var useDatePickerInput : Bool = false
    private var datePickerContainer : UIView?
    private var datePickerConstraintGroupIn : ConstraintGroup?
    private var datePickerConstraintGroupOut : ConstraintGroup?
    private var datePicker : UIPickerView?
    
    var changeCallback : ((textfield : UITextField, range : NSRange, replaceWith : String)->Bool)?
    var startEditingCallback : ((view : CardInfoTextInputView)->Void)?
    var moveScreenCallback : ((keyboardHeight : CGFloat, animationDuration : NSTimeInterval)->Void)?
    var endEditingCallback : ((animationDuration : NSTimeInterval)->Void)?
    
    
    /*****************************
     */
     //MARK: prepare
     /*
     *****************************/
    func prepareTextField (title : String) {
        
        //title
        titleLabel = UILabel()
        self.addSubview(titleLabel!)
        titleLabel!.font = UIFont.systemFontOfSize(12)
        titleLabel!.textColor = UIColor.lightGrayColor()
        titleLabel!.text = title
        constrain (titleLabel!, self) { title, view in
            title.width == view.width
            title.top == view.top
            title.left == view.left
        }
        
        //input
        inputTf = CardInfoTextField()
        self.addSubview(inputTf!)
        inputTf!.font = UIFont.systemFontOfSize(24)
        inputTf!.layer.cornerRadius = 6
        inputTf!.layer.borderWidth = 2
        inputTf!.borderStyle = .Line
        inputTf!.delegate = self
        inputTf!.autocorrectionType = .No
        constrain (inputTf!, titleLabel!) { input, title in
            input.width == title.width
            input.top == title.bottom + 5
            input.left == title.left
        }
        
        //warning
        warningLabel = UILabel()
        self.addSubview(warningLabel!)
        warningLabel!.font = UIFont.systemFontOfSize(12)
        warningLabel!.textColor = UIColor.redColor()
        warningLabel!.textAlignment = .Left
        warningLabel!.hidden = true
        constrain(warningLabel!, inputTf!, self) { warning, input, view in
            warning.left == input.left
            warning.top == input.bottom + 2
            warning.width == input.width
        }
        
        
        
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: "keyboardDoneHandler")
        let space = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: "")
        toolbar.items = [space, doneButton]
        inputTf!.inputAccessoryView = toolbar
        
        
        //hide warning state
        hideWarning()
        
    }
    
    func prepareDatePicker () {
        useDatePickerInput = true
        
        //container
        datePickerContainer = UIView()
        
        //picker
        datePicker = UIPickerView()
        datePickerContainer!.addSubview(datePicker!)
        datePicker!.dataSource = self
        datePicker!.delegate = self
        datePicker!.backgroundColor = UIColor.whiteColor()
        constrain(datePicker!, datePickerContainer!) { picker, container in
            picker.left == container.left
            picker.bottom == container.bottom
            picker.width == container.width
        }
        
        //set current month
        let now = NSDate()
        let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
        let components = calendar!.components(NSCalendarUnit.Month, fromDate: now)
        datePicker!.selectRow(components.month-1, inComponent: 0, animated: false)
        
        updateInputTfWithDate()
        
        
        //toolbar
        let toolbar : UIToolbar = inputTf!.inputAccessoryView! as! UIToolbar
        let rightSpacing = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
        rightSpacing.width = 12
        toolbar.items?.append(rightSpacing)
        datePickerContainer!.addSubview(toolbar)
        constrain(toolbar, datePicker!, datePickerContainer!) { toolbar, picker, container in
            toolbar.bottom == picker.top
            toolbar.left == picker.left
            toolbar.width == container.width
        }
        
    }
    
    
    /*****************************
    */
    //MARK: handlers
    /*
    *****************************/
    
    @objc func keyboardDoneHandler () {
        if !useDatePickerInput {
            inputTf!.resignFirstResponder()
        } else {
            hideDatePicker()
            updateInputTfWithDate()
        }
    }
    
    
    
    /*****************************
     */
     //MARK: actions
     /*
     *****************************/
    
    private func showDatePicker () {
        if !useDatePickerInput {
            return
        }
        
        if let window = UIApplication.sharedApplication().keyWindow, let container = datePickerContainer {
            window.addSubview(container)
            
            if datePickerConstraintGroupOut == nil {
                datePickerConstraintGroupOut = constrain (container, window) { container, window in
                    container.height == window.height
                    container.width == window.width
                    container.left == window.left
                    container.top == window.bottom
                }
                container.layoutIfNeeded()
            }

            
            constrain(clear: datePickerConstraintGroupOut!)
            datePickerConstraintGroupOut = nil
            datePickerConstraintGroupIn = constrain (container, window) { container, window in
                container.edges == window.edges
            }
            
            UIView.animateWithDuration(animationDuration, animations: container.layoutIfNeeded)
            
            //callback
            if let c = moveScreenCallback {
                let toolbar : UIToolbar = inputTf!.inputAccessoryView! as! UIToolbar
                let height = datePicker!.frame.size.height + toolbar.frame.size.height
                c(keyboardHeight: height, animationDuration: animationDuration)
            }
        }
    }
    
    private func hideDatePicker () {
        if !useDatePickerInput {
            return
        }
        
        guard let cgIn = datePickerConstraintGroupIn else {
            return
        }
        
        if let window = UIApplication.sharedApplication().keyWindow, let container = datePickerContainer {
            
            constrain(clear: cgIn)
            datePickerConstraintGroupIn = nil
            
            
            datePickerConstraintGroupOut = constrain (container, window) { container, window in
                container.height == window.height
                container.width == window.width
                container.left == window.left
                container.top == window.bottom
            }
            
            endEditingCallback?(animationDuration: animationDuration)
            
            UIView.animateWithDuration(animationDuration, animations: container.layoutIfNeeded, completion: { animated in
                container.removeFromSuperview()
            })
        }
        
    }
    
    private func updateInputTfWithDate () {
        let month = monthForRow(datePicker!.selectedRowInComponent(0))
        let year = yearForRow(datePicker!.selectedRowInComponent(1))
        inputTf!.text = "\(month)/\(year)"
    }
    
    func showWarning (warning : String) {
        warningLabel?.hidden = false
        warningLabel?.text = warning
        inputTf!.layer.borderColor = UIColor.darkGrayColor().CGColor
        
    }
    
    func hideWarning () {
        warningLabel?.hidden = true
        inputTf!.layer.borderColor = UIColor.darkGrayColor().CGColor
    }
    
    /*****************************
    */
    //MARK: delegate
    /*
    *****************************/
    
    //textfield
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if let c = changeCallback {
            return c(textfield: textField, range: range, replaceWith: string)
        } else {
            return true
        }
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        //callback
        startEditingCallback?(view: self)
        
        //show
        if !useDatePickerInput {
            return true
        } else {
            
            showDatePicker()
            
            return false
        }
    }
    
    
    // picker
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0: //month
            return 12
        case 1: //year
            return 100
        default:
            return 0
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return monthForRow(row)
        case 1:
            return yearForRow(row)
        default:
            return ""
        }
    }
    
    private func monthForRow (row : Int) -> String {
        return String(row+1)
    }
    
    private func yearForRow (row : Int) -> String {
        return String(row+2015)
    }
    
}
