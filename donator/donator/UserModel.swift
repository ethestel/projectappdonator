//
//  UserModel.swift
//  donator
//
//  Created by Onur Ersel on 05/01/16.
//  Copyright © 2016 Onur Ersel. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class UserModel {
    
    static var user : UserModel?
    
    var name : String = ""
    
    class func getUserProfile (callback : (success : Bool, user : UserModel?)->Void) {
        
        //no access token means user did not logged in
        if FBSDKAccessToken.currentAccessToken() == nil {
            callback(success: false, user: nil)
            return
        }
        
        //if user retreived before, return it
        if user != nil {
            callback(success: true, user: user)
            return
        }
        
        if let profile = FBSDKProfile.currentProfile() {
            
            user = UserModel()
            user!.name = profile.name
            callback(success: true, user: user)
            return
            
        } else {
            let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,name"])
            req.startWithCompletionHandler({ (connection : FBSDKGraphRequestConnection!, result : AnyObject!, err : NSError!) -> Void in
                
                if let name = result["name"] as? String {
                    user = UserModel()
                    user!.name = name
                    callback(success: true, user: user)
                } else {
                    callback(success: false, user: nil)
                }
                
                
            })
        }
        
    }
    
    
}
