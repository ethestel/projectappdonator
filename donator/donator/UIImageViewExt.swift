//
//  UIImageViewExt.swift
//  donator
//
//  Created by Onur Ersel on 30/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit


extension UIImageView {
    
    func showImageCallback (success : Bool, image : UIImage?) {
        
        if !success {
            return
        }
        
        self.hidden = false
        self.image = image
        
        self.alpha = 0
        UIView.animateWithDuration(0.1) {
            self.alpha = 1
        }
        
    }
    
}
