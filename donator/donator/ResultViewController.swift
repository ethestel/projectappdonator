//
//  ResultViewController.swift
//  donator
//
//  Created by Onur Ersel on 04/01/16.
//  Copyright © 2016 Onur Ersel. All rights reserved.
//

import UIKit
import Cartography

class ResultViewController : UIViewController {
    
    private var timer : NSTimer?
    
    private var loadingContainer : UIView?
    private var completeContainer : UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepare()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        timer?.invalidate()
        timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "loadingCompleteHandler", userInfo: nil, repeats: false)
        
        showLoading()
    }
    
    
    
    
    /*****************************
    */
    //MARK: prepare
    /*
    *****************************/
    
    
    private func prepare () {
        self.view.backgroundColor = UIColor.whiteColor()
        self.navigationItem.hidesBackButton = true
        
        //loading container
        loadingContainer = UIView()
        self.view.addSubview(loadingContainer!)
        loadingContainer!.hidden = true
        constrain(loadingContainer!, self.view) { container, view in
            container.edges == view.edges
        }
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        loadingContainer?.addSubview(indicator)
        constrain(indicator, loadingContainer!) { indicator, container in
            indicator.center == container.center
        }
        indicator.startAnimating()
        
        
        
        //complete container
        completeContainer = UIView()
        self.view.addSubview(completeContainer!)
        completeContainer!.hidden = true
        constrain(completeContainer!, self.view) { container, view in
            container.edges == view.edges
        }
        
        let resultLabel = UILabel()
        completeContainer!.addSubview(resultLabel)
        resultLabel.font = UIFont.systemFontOfSize(24)
        resultLabel.text = "Payment Completed"
        resultLabel.textColor = UIColor.blackColor()
        constrain(resultLabel, self.view) { label, view in
            label.centerX == view.centerX
            label.centerY == view.centerY - 40
        }
        
        let continueButton = UIButton()
        completeContainer!.addSubview(continueButton)
        continueButton.setTitleColor(self.view.tintColor, forState: .Normal)
        continueButton.setTitle("Back to Organizations", forState: .Normal)
        continueButton.addTarget(self, action: "backHandler", forControlEvents: .TouchUpInside)
        constrain(continueButton, self.view) { button, view in
            button.centerX == view.centerX
            button.centerY == view.centerY + 40
        }
    }
    
    
    private func showLoading () {
        loadingContainer?.hidden = false
        completeContainer?.hidden = true
        
        loadingContainer!.alpha = 0
        UIView.animateWithDuration(0.6) {
            self.loadingContainer!.alpha = 1
        }
    }
    
    private func showComplete () {
        loadingContainer?.hidden = true
        completeContainer?.hidden = false
        
        completeContainer!.alpha = 0
        UIView.animateWithDuration(0.6) {
            self.completeContainer!.alpha = 1
        }
    }
    
    
    /*****************************
    */
    //MARK: handlers
    /*
    *****************************/
    
    @objc func loadingCompleteHandler () {
        timer = nil
        showComplete()
    }
    
    @objc func backHandler () {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    
}