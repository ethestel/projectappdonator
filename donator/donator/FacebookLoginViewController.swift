//
//  FacebookLoginViewController.swift
//  donator
//
//  Created by Onur Ersel on 04/01/16.
//  Copyright © 2016 Onur Ersel. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Cartography

class FacebookLoginViewController : UIViewController, FBSDKLoginButtonDelegate {
    
    
    private var loginButton : FBSDKLoginButton?
    private var nameLabel : UILabel?
    private var currentConstraintGroup : ConstraintGroup?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepare()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "Login"
        checkInformation()
    }
    
    
    
    
    
    /*****************************
    */
    //MARK: prepare
    /*
    *****************************/
    
    private func prepare () {
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        //login button
        loginButton = FBSDKLoginButton()
        self.view.addSubview(loginButton!)
        loginButton!.delegate = self
        loginButton!.readPermissions = ["public_profile"]
        
        //name
        nameLabel =  UILabel()
        self.view.addSubview(nameLabel!)
        nameLabel!.font = UIFont.systemFontOfSize(24)
        nameLabel!.numberOfLines = 2
        nameLabel!.textAlignment = .Center
        nameLabel!.hidden = true
        
        constrain (loginButton!, nameLabel!, self.view) { login, name, view in
            login.centerX == view.centerX
            name.centerX == view.centerX
        }
    }
    
    private func checkInformation () {
        
        UserModel.getUserProfile { (success : Bool, user : UserModel?) -> Void in
            
            //clear old constraints
            if let cg = self.currentConstraintGroup {
                constrain (clear : cg)
            }
            
            self.view.layoutIfNeeded()
            
            
            if success, let u = user {
                
                self.nameLabel!.text = "Hello,\n\(u.name)"
                self.nameLabel!.hidden = false
                self.nameLabel!.alpha = 0
                self.loginButton!.alpha = 0
                
                self.currentConstraintGroup = constrain(self.nameLabel!, self.loginButton!, self.view) { label, login, view in
                    label.centerY == view.centerY - 20
                    login.top == label.bottom + 20
                }
                
                UIView.animateWithDuration(0.5, animations: {
                    self.nameLabel!.alpha = 1
                    self.loginButton!.alpha = 1
                })
                
            } else {
                
                self.nameLabel!.alpha = 1
                
                self.currentConstraintGroup = constrain(self.nameLabel!, self.loginButton!, self.view) { label, login, view in
                    login.centerY == view.centerY
                }
                
                UIView.animateWithDuration(0.5, animations: {
                    self.nameLabel!.alpha = 0
                }, completion:  { finished in
                    self.nameLabel!.hidden = true
                })

            }
            
        }
        
    }
    
    
    
    /*****************************
    */
    //MARK: delegate
    /*
    *****************************/
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        checkInformation()
    }
    
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        checkInformation()
    }
    
    
}
