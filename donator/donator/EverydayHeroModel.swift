//
//  EverydayHeroModel.swift
//  donator
//
//  Created by Onur Ersel on 24/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit
import Alamofire

class EverydayHeroModel {
    
    //static let url = "https://edheroz.com"
    static let url = "https://everydayhero.com"
    
    class func getCharities(page pageInternal : Int, limit : Int, callback : ((success:Bool, charities:[CharityModel]?)->Void)) -> Void {
        
        Alamofire.request(.GET, "\(url)/api/v2/charities?limit=\(limit)&page=\(pageInternal)")
            .responseJSON(completionHandler: { response in
                
                if let json = response.result.value as? NSDictionary {
                    var charities = [CharityModel]()
                    
                    if let arr = json["charities"] as? NSArray {
                        
                        //prepare charities array
                        for jsonCharity in arr {
                            if let jsc = jsonCharity as? NSDictionary {
                                let c = CharityModel(json: jsc)
                                charities.append(c)
                            }
                        }
                        
                    }
                    
                    callback(success: true, charities: charities)
                } else {
                    callback(success: false, charities: nil)
                }
            
        })
        
    }
    
    
    
}
