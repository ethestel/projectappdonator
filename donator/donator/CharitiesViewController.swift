//
//  CharitiesViewController.swift
//  donator
//
//  Created by Onur Ersel on 23/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit
import Cartography

class CharitiesViewController : UITableViewController {
    
    let cellIndetifier = "charities_cell_identifier"
    var didLoaded = false
    
    private enum LoadingState {
        case Idle, Initial, Refresh, More
    }
    
    private var loadMoreIndicator : UIActivityIndicatorView?
    private var charityCollection : CharityCollectionModel?
    private var loadingState : LoadingState = .Idle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareRefreshControl()
        prepareCharityCollection()
        prepareFooterView()
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //navigation bar
        let nav = self.navigationController! as! SlidingMenuNavigationController
        self.navigationItem.leftBarButtonItem = nav.hamburgerButton!
        self.navigationItem.title = "GiveEasy"
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !didLoaded {
            beginLoadingInitialData()
        }
        
    }
    
    
    /*****************************
    */
    //MARK: prepare
    /*
    *****************************/
    
    private func prepareRefreshControl () {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: "refreshHandler", forControlEvents: .ValueChanged)
    }
    
    private func prepareCharityCollection () {
        
        charityCollection = CharityCollectionModel(limit: 30)
    }
    
    private func prepareFooterView () {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 0, height: 40)
        self.tableView.tableFooterView = view
        
        loadMoreIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        view.addSubview(loadMoreIndicator!)
        
        constrain (loadMoreIndicator!, view) { loadMoreIndicator, view in
            loadMoreIndicator.center == view.center
        }
        
        self.tableView.contentInset.bottom = -self.tableView.tableFooterView!.frame.height
    }

    /*****************************
     */
     //MARK: animations
     /*
     *****************************/
     
    private func showLoadMoreIndicator () {
        
        if loadMoreIndicator!.isAnimating() {
            return
        }
        
        loadMoreIndicator?.startAnimating()
        loadMoreIndicator?.transform = CGAffineTransformMakeScale(0,0)
        
        UIView.animateWithDuration(0.2) {
            self.loadMoreIndicator?.transform = CGAffineTransformMakeScale(1, 1)
        }
    }
    
    private func hideLoadMoreIndicator () {
        
        if !loadMoreIndicator!.isAnimating() {
            return
        }
        
        loadMoreIndicator?.stopAnimating()
    }
    
    
    
    /*****************************
     */
     //MARK: data
     /*
     *****************************/
    
    private func loadNextPage (callback : (success : Bool)->Void) {
        EverydayHeroModel.getCharities(page: charityCollection!.nextPage, limit: charityCollection!.limit) { (success, charities) -> Void in
            if success, let cs = charities {
                self.charityCollection!.addPage(cs)
            }
            
            callback(success: success)
        }
    }
     
    //initial loading
    private func beginLoadingInitialData () {
        
        //checking state
        if loadingState != .Idle {
            return
        }
        loadingState = .Initial
        
        //load
        LoadingIndicatorView.show()
        self.tableView.tableFooterView?.hidden = true
        
        loadNextPage { (success) -> Void in
            
            if success {
                self.tableView.reloadData()
            } else {
                self.failInitialLoad()
            }
            
            LoadingIndicatorView.hide()
            self.tableView.tableFooterView?.hidden = false
            self.loadingState = .Idle
            self.didLoaded = true
        }
    }
    
    //top pull to refresh
    private func beginRefreshingData () {
        
        //checking state
        if loadingState != .Idle {
            return
        }
        loadingState = .Refresh
        
        //refresh
        charityCollection!.reset()
        
        loadNextPage { (success) -> Void in
            self.refreshControl?.endRefreshing()
            
            if success {
                self.tableView.reloadData()
            }
            
            self.loadingState = .Idle
        }
    }
    
    //down load more 
    private func loadMore () {
        
        //checking state
        if loadingState != .Idle {
            return
        }
        loadingState = .More
        
        
        //load
        self.tableView.contentInset.bottom = 0
        showLoadMoreIndicator()
        
        loadNextPage { (success) -> Void in
            
            if success {
                self.tableView.reloadData()
            }
            
            self.loadingState = .Idle
            self.tableView.contentInset.bottom = -self.tableView.tableFooterView!.frame.height
            self.hideLoadMoreIndicator()
            
        }
    }
    
    
    /*****************************
     */
     //MARK: fail
     /*
     *****************************/
     
    private func failInitialLoad () {
        
    }
    
    private func failLoadingData () {
        
    }
    
    
    /*****************************
    */
    //MARK: handlers
    /*
    *****************************/
    
    @objc func refreshHandler () {
        beginRefreshingData()
    }
    
    /*****************************
    */
    //MARK: delegates
    /*
    *****************************/
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.charityCollection!.charities.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIndetifier) as? CharityCell
        if cell == nil {
            cell = CharityCell(style: .Default, reuseIdentifier: cellIndetifier)
            cell!.prepare()
        }
        
        if let model = charityCollection!.getCharityWithIndexPath(indexPath) {
            cell!.assignModel(model)
        }
        
        return cell!
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        //dont refresh if all rows dont fill screen
        if scrollView.contentSize.height < scrollView.frame.size.height {
            return
        }
        
        let contentBottomOffset = scrollView.frame.size.height - (scrollView.contentSize.height + scrollView.contentInset.bottom - scrollView.contentOffset.y)
        
        if contentBottomOffset > 50 {
            loadMore()
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return CharityCell.height
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector("setSeparatorInset:") {
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        if cell.respondsToSelector("setPreservesSuperviewLayoutMargins:") {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.respondsToSelector("setLayoutMargins:") {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        if let model = charityCollection!.getCharityWithIndexPath(indexPath) {
            let vc = CharityDetailsViewController()
            vc.assignModel(model)
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
}
