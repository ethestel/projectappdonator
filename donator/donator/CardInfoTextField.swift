//
//  CardInfoTextField.swift
//  donator
//
//  Created by Onur Ersel on 30/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit

class CardInfoTextField: UITextField {
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds, 10, 10)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds, 10, 10)
    }
    
}
