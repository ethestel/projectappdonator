//
//  Cause.swift
//  donator
//
//  Created by Onur Ersel on 24/12/15.
//  Copyright © 2015 Onur Ersel. All rights reserved.
//

import UIKit

struct Cause {
    
    let id : Int
    let key : String
    let name : String
    
    init () {
        id = 0
        key = ""
        name = ""
    }
    
    init (params : NSDictionary) {
        id = (params["id"] as? Int  ??  0)
        key = (params["key"] as? String  ??  "")
        name = (params["name"] as? String  ??  "")
    }
}
